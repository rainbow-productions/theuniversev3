// pages/api/redirect.js
import { NextResponse } from 'next/server';

export default function handler(req, res) {
  try {
    // Get the current path.
    const path = req.nextUrl.pathname;

    // Redirect the user to the current path.
    const response = NextResponse.redirect(path);

    // Return the response.
    return response;
  } catch (error) {
    // Handle the error.
    console.error(error);

    // Return a 500 error response.
    return new Response('An error occured... Use it properlu or you have no balls.', {
      status: 500,
      headers: {
        'Content-Type': 'text/plain',
      },
    });
  }
}
