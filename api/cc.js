// pages/api/clear-cookies.js
import { NextResponse } from 'next/server';

export default function handler(req, res) {
  // Get all cookies.
  const cookies = req.cookies;

  // Clear all cookies.
  for (const cookieName in cookies) {
    res.clearCookie(cookieName);
  }

  // Redirect the user to the home page.
  const response = NextResponse.redirect('/');

  // Return the response.
  return response;
}
