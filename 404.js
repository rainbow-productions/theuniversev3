// 404.js
// This file is used to handle 404 errors in Vercel deployments.

// Get the environment variables.
const { NODE_ENV, VERCEL_ENV } = process.env;

// Set the default 404 page.
let page = '/404.html';

// If the environment is production, set the 404 page to a more generic page.
if (NODE_ENV === 'production' || VERCEL_ENV === 'production') {
  page = '/404';
}

// Export the 404 page.
export default page;
